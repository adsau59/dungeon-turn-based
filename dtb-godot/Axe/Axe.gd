"""
Axe Pickup Item
"""

extends MeshInstance
class_name Axe

export(Vector2) var onTile: Vector2
export(int) var durability: int = 5


"""
Used to tag object as a pickup
"""
func i_am_pickup():
	pass

"""
Connected to `_on_area_area_entered` signal of Area Node
when player collides with pickup, play sound and add weapon, delete pickup item
"""
func _on_area_area_entered(area):
	if area.get_parent() is Player:
		SFX.PlayPickup()
		area.get_parent().attackDurability += durability
		queue_free()
