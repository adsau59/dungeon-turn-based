"""
FaceRotate

depending on the object facing direction, rotate it
connected to `faceDirChanged` signal in `AI` and `Controllable`
"""

extends Node

func changedFacingDir(val):
	var deg = 0
	match val:
		Vector2(1,0):
			deg = -90
		Vector2(-1,0):
			deg = 90
		Vector2(0,1):
			deg = 180
		Vector2(0,-1):
			deg = 0
	get_parent().rotation_degrees.y = deg