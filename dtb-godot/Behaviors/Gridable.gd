"""
Gridable Behaviour

Makes the object placable on grid, 
enables it to move/snap from one tile to other
"""

extends Node
class_name Gridable

"""
Signaled move animation is complete
"""
signal moved

"""
Animation speed to move from one tile to another
"""
export(float) var moveSpeed: float = 3

"""
Used to calculate the spaces between each tile centers
same as in `TileSystem`
"""
var gridToPosFactor :float = TileSystem.gridToPosFactor

"""
Nomalized time of how much the animation of moving is completed
"""
var t: float = 1

"""
Cache for moving animation
"""
var fromMove: Vector3
var toMove: Vector3

"""
Cache for parent node
"""
var parent

#	@PUBLIC

"""
Snap to assigned grid tile when the game starts
"""
func _ready():
	parent = get_parent()
	snapTo(parent.onTile)
	set_physics_process(false)

"""
Gets the translation of grid tile location
"""
func GetPosFromGrid(gridLoc: Vector2) -> Vector3:
	return Vector3(gridToPosFactor * gridLoc.x, get_parent().translation.y, gridToPosFactor * gridLoc.y)

"""
snap to tile relative to the tile object is on
"""
func snapFrom(dir: Vector2) -> bool:
	return snapTo(parent.onTile + dir)

"""
snap to target tile location
"""
func snapTo(tile: Vector2):
	if !TileSystem.IsMovable(tile):
		return false
	
	get_parent().translation = GetPosFromGrid(tile)
	setOnTile(tile)
	return true
	

"""
Move (with animation) to tile relative to the tile object is on
"""
func moveFrom(dir: Vector2) -> bool:
	return moveTo(parent.onTile + dir)


"""
Move (with animation) to target tile location
"""
func moveTo(tile: Vector2) -> bool:
	if !TileSystem.IsMovable(tile):
		return false
		
	if(t < 1):
		return false
	fromMove = GetPosFromGrid(parent.onTile)
	toMove = GetPosFromGrid(tile)
	t = 0
	setOnTile(tile)
	set_physics_process(true)
	return true

#	@PRIVATE

"""
Updating TileSystem cache with the new occupied tile location,
updating `onTile` cache in parent
"""
func setOnTile(tileLoc):
	TileSystem.GetTile(parent.onTile).Leave()
	parent.onTile = tileLoc
	TileSystem.GetTile(tileLoc).Occupy(parent)

"""
Returns the tile location relative to the current tile
"""
func getTileLocInDir(dir: Vector2):
	return parent.onTile + dir
	
#	@GODOT

"""
Animates moving from mone tile to another,
when animation is completed, it is disabled
"""
func _physics_process(delta):
	get_parent().translation = fromMove.linear_interpolate(toMove, t)
	t += delta * moveSpeed
	if t >= 1:
		set_physics_process(false)
		emit_signal("moved")