"""
Turnable Behaviour

Enables an object to play turns, when initialized in their script
"""
extends Node

class_name Turnable

"""
true its parent's turn
used to check if its player turn before accepting input
"""
var myTurn: bool = false

"""
cache of parent object
"""
var obj

# @PUBLIC

"""
complete selfs turn
"""
func useMove():
	TurnSystem.UseMove(self)

# @GODOT

"""
Constructor

initializes the Turnable object and adds it into TurnSystem
"""
func _init(_obj, isFirst:bool = false):
	obj = _obj
	myTurn = isFirst
	TurnSystem.AddTurnables(self)