extends Camera

export(bool) var follow: bool = true
onready var player = get_node("/root/Game/Player")
var offset: Vector3

func _ready():
	offset = self.translation- player.translation
	set_process(follow)
	
func _process(delta):
	call_deferred("follow_camera", delta)
	
func follow_camera(delta):
	var target = Vector3(player.translation.x + offset.x, translation.y, player.translation.z + offset.z)
	self.translation = translation.linear_interpolate(target, delta)