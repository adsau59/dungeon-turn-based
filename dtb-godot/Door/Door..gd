"""
Door Script

Blocks the path, but can be removed by player
"""

extends Spatial
class_name Door

"""
On what tile the door is, used by `Gridable` child node
"""
export(Vector2) var onTile: Vector2

"""
Defines the orientation of the door, true if vertical
"""
export(bool) var orientation: bool

"""
true if open, if true doesnt let anyone pass
"""
var isOpen: bool = false

"""
used to tag door
"""
func i_am_door():
	pass

"""
Plays sound, changes `isOpen` to true, removes door model
"""
func OpenDoor():
	SFX.PlayDoorunlocked()
	isOpen = true
	$Door.queue_free()
	$area3.queue_free()