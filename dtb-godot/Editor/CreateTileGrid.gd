"""
Editor script used to create wall and tiles
"""

tool
extends Node

"""
Size of the level to start designing
"""
export(Vector2) var gridSize:Vector2 = Vector2(2,2)

"""
Check this box before checking other to run the script
"""
export(bool) var runScript

"""
Creates a grid of tiles, gridSize.x * grideSize.y
"""
export(bool) var createGrid = false setget create_grid

"""
Calculates and places, walls and pillars
"""
export(bool) var createWall = false setget create_wall_border

#	@PUBLIC

"""
Creates a grid of tiles, gridSize.x * grideSize.y
"""
func create_grid(new_val):
	# check if runScript is checked before running code to avoid accidental execution
	if !runScript:
		return
	runScript = false
	
	# load tile prefab scene
	var tileScene = load("res://Tile/tile.tscn")
	
	# delete the existing tiles to avoid duplication
	for f in get_children():
		f.queue_free()
	
	# loop through each tile location and instantiate a tile
	for i in range(gridSize.x):
		for j in range(gridSize.y):
			var tile = tileScene.instance()
			add_child(tile)
			tile.set_owner(owner)
			tile.snapTo(Vector2(i,j))
			
	# reset the bool to false just so it doesn't get a tick in the inspector
	createGrid = false

"""
Calculates and places walls, also activates pillars accordingly
"""
func create_wall_border(val):
	# check if runScript is checked before running code to avoid accidental execution
	if !runScript:
		return
	runScript = false
		
	# initialize variables which will be used in calculations
	
	# all the tiles nodes
	var tiles = get_children()
	# wall prefab scene
	var wallScene = load("res://Wall/borderwall.tscn")
	# wall parent to keep all walls organized in a node
	var borderwallsParent = get_node("../borderwalls")
	# possible directions for a tile
	var neighbourDirs = [Vector2(1,0), Vector2(0,1), Vector2(-1,0), Vector2(0,-1)]
	
	# delete all the walls that previously exists to avoid
	for w in borderwallsParent.get_children():
		w.queue_free()
	
	for t in tiles:
		# for each tile, check in which direction pillar have to be places
		var cornerDirs = getCornerDirs(tiles, t)
		for n in neighbourDirs:
			# if there is nothing in the direction where the wall has to be placed
			if !hasNeighborInDir(tiles, t, n):
				#spawn a wall
				var w = spawnWallInDir(wallScene, borderwallsParent, t, n)
				
				# for each neighbor check if a pillar is to be activated in the wall
				for c in cornerDirs:
					# find the correct cornerDir (n: neightborDir)
					if c[0] == n:
						# which pillar to activate
						if !c[1]:
							w.get_node("Pillar").visible = true
							w.pillar1Visible = true
						else:
							w.get_node("Pillar2").visible = true
							w.pillar2Visible = true
	
	# reset the bool to false just so it doesn't get a tick in the inspector						
	createWall = false
	
#	@PRIVATE
					
"""
Finds out all the directions where a pillar has to be placed for a tile
@param tiles: [tile]; array of 
"""
func getCornerDirs(tiles, tile):
	"""
	pillar data: [[wallDir, pillar2]...]; data needed to activate pillars for tiles
	wallDir: Vector2; for which wall pillar has to be activate
	pillar2: bool; says which pillar to activate
	"""
	var t = []
	t.clear()
		
	checkCornerPillar(tiles, t, tile, Vector2(1,0), 	Vector2(0,1), 	Vector2(1,0), 	false)
	checkCornerPillar(tiles, t, tile, Vector2(-1,0), 	Vector2(0,1), 	Vector2(0,1), 	false)
	checkCornerPillar(tiles, t, tile, Vector2(-1,0), 	Vector2(0,-1), 	Vector2(0,-1), 	false)
	checkCornerPillar(tiles, t, tile, Vector2(1,0), 	Vector2(0,-1), 	Vector2(1,0), 	true)
		
	checkLPillar(tiles, t, tile, Vector2(-1,0), Vector2(0,1), 	Vector2(0,1), 	false)
	checkLPillar(tiles, t, tile, Vector2(1,0), 	Vector2(0,1), 	Vector2(0,1), 	true)
	checkLPillar(tiles, t, tile, Vector2(-1,0), Vector2(0,-1), 	Vector2(0,-1), 	false)
	checkLPillar(tiles, t, tile, Vector2(1,0), 	Vector2(0,-1), 	Vector2(0,-1), 	true)
	
	return t

"""
Checks if the tile has L corner (inward)
"""
func checkLPillar(tiles, pillarDatas, tile, d1, d2, wallDir, pillar2):
	if hasNeighborInDir(tiles, tile, d1) &&\
	hasNeighborInDir(tiles, GetTile(tiles, tile.tileLoc+ d1), d2) &&\
	!hasNeighborInDir(tiles, tile, d2):
		pillarDatas.push_back([wallDir, pillar2])
		
"""
Checks if the tile has˥ corner (outward)
"""
func checkCornerPillar(tiles, pillarDatas, tile, d1, d2, wallDir, pillar2):
	if !hasNeighborInDir(tiles, tile, d1) &&\
	!hasNeighborInDir(tiles, tile, d2):
		pillarDatas.push_back([wallDir, pillar2])

"""
Checks if the tile has a neigbour in direction
@param tiles: [tile-obj]; list of all the tiles
@param tile: SpacialNode; tile object
@param dir: Vector2
"""
func hasNeighborInDir(tiles, tile, dir:Vector2):
	for i in range(tiles.size()):
		if tiles[i].tileLoc == tile.tileLoc + dir:
			return true		
	return false

"""
Checks if the tile has a neigbour in direction
@param tiles: [tile-obj] list of all the tiles
@param tile: Vector2; tile location
@param dir: Vector2
"""
func hasNeighborInDirVector(tiles, tile, dir:Vector2):
	for i in range(tiles.size()):
		if tiles[i].tileLoc == tile + dir:
			return true
			
	return false

"""
Find tile object by tile location
"""
func GetTile(tiles, loc):
	for t in tiles:
		if t.tileLoc == loc:
			return t
			
	return null

"""
Instantiates a wall in direction
@param wallScene prefab scene of the wall
@param borderwallsParent wall node
@param tile target tile to spawn wall on
@param dir direction of the wall, adds an offset in that direction for the wall
"""
func spawnWallInDir(wallScene, borderwallsParent, tile, dir:Vector2):
	var wall = wallScene.instance()
	wall.translation = tile.translation + Vector3(dir.x * 5, 0, dir.y * 5)
	borderwallsParent.add_child(wall)
	wall.set_owner(borderwallsParent.owner)
	wall.orientation = dir.x == 0
	
	return wall
	
	
	
	