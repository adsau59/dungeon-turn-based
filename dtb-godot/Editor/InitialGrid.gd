"""
InitialGrid Editor

Changes the `onTile` of parent depending on where the object is placed 
"""
tool
extends Node

"""
Used to compare objects
"""
const Infinity = 3.402823e+38

"""
copies the parent translation to get a callback when it is changed
"""
var pos: Vector3 setget pos_changed

"""
array of all the tiles
"""
var tiles: Array

#	@PRIVATE

"""
Calculates the nearest tile
"""
func GetNearestTile(pos: Vector3) -> Vector2:
	
	tiles =  get_node("../../Grid").get_children()
	
	var dist = Infinity
	var nearestTile = null
	for t in tiles:
		if pos.distance_to(t.translation) < dist:
			nearestTile = t
			dist = pos.distance_to(t.translation)
	return nearestTile.tileLoc

#	@GODOT

"""
keeps updating the translation of parent with `pos`
so that `pos_changed` is executed when its changed
"""
func _physics_process(delta):
	if !Engine.editor_hint:
		return
	
	if pos != get_parent().translation:
		self.pos = get_parent().translation
	
"""
Calculates and updates the `onTile` when translation is updated
"""
func pos_changed(val):
	if get_parent() == null:
		return
	
	get_parent().onTile = GetNearestTile(get_parent().translation)
	pos = val
