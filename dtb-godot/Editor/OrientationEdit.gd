"""
Orientation updater editor

Updates the orientation of the object when bool in the parent is updated
"""

tool
extends Node

"""
copies the parent orientation to get callback when it is changed
"""
var orientation: bool setget orientation_changed


#	@PRIVATE

"""
rotates the parent object depending on the orientation bool of it
"""
func orientation_changed(val):
	if val == false:
		get_parent().rotation_degrees.y = 90
	else:
		get_parent().rotation_degrees.y = 0
	orientation = val
	
#	@GODOT

"""
keeps updating the orientation of the object when it is changed
"""
func _physics_process(delta):
	if !Engine.editor_hint:
		return
	
	if orientation != get_parent().orientation:
		self.orientation = get_parent().orientation
	
