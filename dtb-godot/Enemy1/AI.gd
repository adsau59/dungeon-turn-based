"""
AI Script

Basic enemy AI Script,
Child of the Enemy node,
controls the movement and the turns of the ai
"""

extends Node

"""
Signaled when facing direction of the enemy is changed
to update the rotation of the parent objects
"""
signal faceDirChanged

"""
Direction where the enemy is facing
"""
var facingDir: Vector2 = Vector2(0,-1) setget setFacingDir

"""
Player node cache
"""
var player: Player

"""
true if player is detected
"""
var playerFound:bool setget agro_changed

"""
true if enemy is knocked out for a turn
"""
var knocked: bool = false

"""
Connects `makeMove` method to `TurnChanged` signal in `TurnSystem`
Connects `moved` mehtod to `moved` signal in `Gridable` sibling node
initiate player cache and facingDir depending on the `verticalPatrol` in parent
"""
func _ready():
	TurnSystem.connect("TurnChanged", self, "makeMove")
	get_parent().get_node("Gridable").connect("moved", self, "moved")
	player = get_node("/root/Game/Player")
	
	if get_parent().verticalPatrol:
		facingDir = Vector2(0,1)
	else:
		facingDir = Vector2(1,0)

"""
Executed when turn is changed,
plays its turn then uses turnable to finish move
"""
func makeMove(turnable):
	
	# checks if its my turn
	if turnable != get_parent().turnable:
		return
	
	# skip turn if knocked out
	if knocked:
		knocked = false
		moved()
		return
	
	# if player is detected
	if playerFound:
		# calculate the direction player is in
		self.facingDir = calculateDir()
		
		# if player escaped the LOS change `playerfound` and restart the function
		if !is_player_insight():
			self.playerFound = false
			makeMove(turnable)
			return
		
		# if reached player then damage it and finish turn
		if getFacingTile().occupiedBy is Player && !get_parent().isDead():
			player.hurt()
			moved()
			return
			
	# if not returned before, move towards the calculated direction
	# checks if the Enemy is on screen to play animation
	if get_node("../VisibilityNotifier").is_on_screen():
		if !get_parent().gridable.moveFrom(self.facingDir):
			moved()
	else:
		get_parent().gridable.snapFrom(self.facingDir)
		moved()
"""		
finishes the turn and then checks for player on detection tile
"""
func moved():
	
	# clear the old detection tiles
	get_parent().disableDetectionTiles()
	
	# if player isn't detected, look for it
	if !playerFound:
		
		# if reached a dead end, turn around
		if !TileSystem.IsMovable(get_parent().gridable.getTileLocInDir(facingDir)):
			self.facingDir *= -1
			
		# enable the detection tiles
		get_parent().enableDetectionTiles(getTilesToCheck())
		
		# check for player in the detection tiles, and also see if its in LOS
		if checkForPlayer() && is_player_insight():
			self.playerFound = true
			
	# finishes the turn
	get_parent().turnable.useMove()
	

"""
Get the tile where the enemy is facing
"""
func getFacingTile() -> Tile:
	return TileSystem.GetTile(get_parent().gridable.getTileLocInDir(facingDir))

"""
Calculates the detection tile
@return [Vector2] array of tile location which has to detected
"""
func getTilesToCheck() -> Array:
	var list : Array
	list.clear()
	var facingTile = get_parent().gridable.getTileLocInDir(facingDir)
	list.push_back(facingTile)
	facingTile += facingDir
	for i in range(get_parent().detectionRange - 1):
		for j in range(-1,2):
			var tile = Vector2(\
			facingTile.x + (i * facingDir.x + j * facingDir.y),\
			facingTile.y + (i * facingDir.y + j * facingDir.x))
			
			if TileSystem.IsMovable(tile):
				list.push_back(tile)
				
	return list

"""
check for player in the detection tiles
@return true if player found
"""
func checkForPlayer() -> bool:
	var tiles = getTilesToCheck()
	
	for t in tiles:
		if TileSystem.GetTile(t).occupiedBy is Player:
			return true
	
	return false

"""
Calculate the direction in which player is
@return Vector2 direction to move where player is
"""
func calculateDir() -> Vector2:
	var xDir = sign(player.onTile.x - get_parent().onTile.x)
	var yDir = sign(player.onTile.y - get_parent().onTile.y)
	
	if !TileSystem.IsMovable(get_parent().gridable.getTileLocInDir(Vector2(xDir,0))):
		xDir = 0
	
	if !TileSystem.IsMovable(get_parent().gridable.getTileLocInDir(Vector2(0,yDir))):
		yDir = 0
	
	if xDir != 0 && yDir != 0:
		randomize()
		if rand_range(0,2) > 0:
			xDir = 0
		else:
			yDir = 0	
			
	return Vector2(xDir, yDir)	

"""
check if the plyaer is in LOS
@return bool true if player is detected
"""
func is_player_insight() -> bool:
	var space_state = get_parent().get_world().direct_space_state
	var selection = space_state.intersect_ray(get_parent().translation, player.translation, [], 2147483647, true, true)
	
	return selection.collider.get_parent() is Player

"""
setter callback when `facingDir` variable is changed
"""
func setFacingDir(val):
	emit_signal("faceDirChanged", val)
	facingDir = val

"""
setter callback when `playerFound` is changed
changes the model of enemy
"""
func agro_changed(val):
	get_node("../NormalMesh").visible = !val
	get_node("../AgroMesh").visible = val
	playerFound = val