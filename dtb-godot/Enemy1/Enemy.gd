"""
Enemy Script

Mostly contains enemies properties and methods which changes them
"""
extends Spatial
class_name Enemy

"""
on what tile the enemy is standing on
"""
export(Vector2) var onTile: Vector2

"""
true if the enemy will initially patrol up and down
"""
export(bool) var verticalPatrol: bool = true

"""
Initializing turnable behaviour, enables object to play turn
"""
onready var turnable: Turnable = Turnable.new(self)

"""
Caching `Gridable` child node
"""
onready var gridable: Gridable = $Gridable

"""
how far will the enemy can see to detect the plyaer
"""
export(int) var detectionRange: int = 3

"""
health of enemy, when reaches zero enemy dies
"""
export(int) var health :int = 1

"""
facing direction of the enemy, managed by `AI` node
"""
var facingDir: Vector2

"""
detection tiles array, managed by `AI` node
"""
var detectionTiles =[]

"""
Connects the `changedFacingDir` method to `faceDirChanged` signal in `AI` node
"""
func _ready():
	$AI.connect("faceDirChanged", $FaceRotate, "changedFacingDir")

#	@PUBLIC

"""
used to check if the object is an enemy
"""
func i_am_enemy():
	pass

"""
called by player when attacked, 
reduces the health by damage,
if health falls below 1 then deletes the enemy and disables the detection tiles
"""
func hurt(damage:int):
	health -= damage
	if health <= 0:
		disableDetectionTiles()
		call_deferred("queue_free")
		
"""
called by player when knocks back, turn `knockback` bool in AI to true
moves the AI back
"""
func knockback(dir):
	$AI.knocked = true
	gridable.snapFrom(dir)
	
"""
returns true if dead
"""
func isDead():
	return health <= 0

"""
clears the cached tiles then updates it with new one
changes the model of detection tiles to red
@param tiles [tile] Array of tile objects to update detectionTiles array
"""	
func enableDetectionTiles(tiles):
	
	detectionTiles.clear()
	detectionTiles += tiles
	
	for t in detectionTiles:
		var tile = TileSystem.GetTile(t)
		
		if TileSystem.IsMovableTile((tile)):
			tile.detectionTile(true)
	
"""
changes the model of old detection tile to normal color
clears the detectionTiles Array
"""
func disableDetectionTiles():
	for t in detectionTiles:
		var tile = TileSystem.GetTile(t)
		tile.detectionTile(false)
	detectionTiles.clear()