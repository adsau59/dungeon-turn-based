"""
Key Script
"""

extends Spatial
class_name Key

"""
Says on which tile the object is on
"""
export(Vector2) var onTile: Vector2

"""
used to check if the object is a pickup item
"""
func i_am_pickup():
	pass

"""
when the player collides, increase their keys count and destory
"""
func _on_area_area_entered(area):
	if area.get_parent() is Player:
		SFX.PlayPickup()
		area.get_parent().keys += 1
		queue_free()
