extends Control


"""
shows the help screen
"""
func _play_pressed():
	$help.show()

"""
Quits the game
"""
func _quit_pressed():
	get_tree().quit()