extends Control

"""
restart the level
"""
func _replay_pressed():
	TileSystem.Restart()

"""
quit the game
"""
func _quit_pressed():
	get_tree().quit()
