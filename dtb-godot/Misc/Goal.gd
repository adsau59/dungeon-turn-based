"""
Goal script

shows the win screen when collided
"""
extends Spatial

"""
on what tile the object is on
"""
export(Vector2) var onTile:Vector2

"""
connects the collision signal to `showWin` method
"""
func _ready():
	$area2.connect("area_entered", self, "showWin")

"""
used to check if the object is a pickup item
"""
func i_am_pickup():
	pass

"""
Show the win screen
"""
func showWin(area:Area):
	if area.get_parent() is Player:
		get_node("/root/Game/HUD/WinScreen").show()