"""
HUD Script

Manages HUD elements
"""
extends Control

"""
Cache of heart image nodes in the hud
"""
var hearts: Array

"""
Cache of durabilities image nodes in the hud
"""
var durabilities :Array

"""
Cache of key image nodes in the hud
"""
var key: TextureRect

"""
Called when player takes damage
plays red pulse animation
"""
func red_pulse():
	$ColorRect.show()
	$ColorRect/Anim.play("hurt_pulse")

"""
hides the red pulse rect when the animation finishes to avoid input interference
Connected by signal to animation
"""
func _red_pulse_finish(anim_name):
	$ColorRect.hide()

"""
Restart the game when retry button is pressed,
connected via signal to button
"""
func _retry_pressed():
	TileSystem.Retry()

"""
Called by player when they take damage to update hearts
"""
# todo opitimize use signals instead
func set_hearts(amount:int):
	if hearts.size() == 0:
		hearts = $Main/Stats/hearts.get_children()
	
	for h in hearts:
		h.hide()
	for i in range(amount):
		hearts[i].show()

"""
Called by player when weapon is used
"""
# todo optimize use signals instead
func set_durability(amount:int):
	if durabilities.size() == 0:
		durabilities = $Main/Stats/durabilty.get_children()
	
	for d in durabilities:
		d.hide()
	for i in range(amount):
		durabilities[i].show()
		
"""
Called by player when they take damage
"""
# todo optimize use signals instead
func set_key(val:bool):
	
	if key == null:
		key = $Main/Stats/key
	
	if val:
		key.show()
	else:
		key.hide()

"""
called when player dies,
shows your dead screen
"""
# todo optimize use signals instead
func showRetry():
	$RetryScreen.show()