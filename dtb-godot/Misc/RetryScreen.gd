extends Control

"""
Plays fade in animation
"""
func show():
	$AnimationPlayer.play("fade_in")

"""
Restarts the level
"""
func _retry_pressed():
	TileSystem.Retry()
