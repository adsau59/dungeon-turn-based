"""
Controllable script

makes the player controllable
allows to do things when its players turn
"""
extends Spatial

"""
Signaled when control is pressed and dir to be moved is calculated
"""
signal move(dir)

"""
Signaled when direction is changed, to update the rotation of the parent
"""
signal faceDirChanged

#	@PRIVATE

"""
Checks for the correct action (open door, attack enemy, move)
then perform it
"""
func move_ctrl(dir):
	var tile = TileSystem.GetTile(get_parent().gridable.getTileLocInDir(dir))

	emit_signal("faceDirChanged", dir)

	#open door
	if tile != null &&\
	tile.IsOccupied() && \
	tile.occupiedBy.has_method("i_am_door") && \
	get_parent().keys > 0 &&\
	!tile.occupiedBy.isOpen:
		tile.occupiedBy.OpenDoor()
		get_parent().turnable.useMove()
		get_parent().keys-=1
		return
			
	#attack enemy
	if tile != null &&\
	tile.IsOccupied() && \
	tile.occupiedBy is Enemy &&\
	get_parent().attackDurability > 0:
		get_parent().damage(tile.occupiedBy)
		get_parent().turnable.useMove()
		return
	
	# todo dont use signal for internal use
	emit_signal("move", dir)
	
#	@PUBLIC

"""
Read input if players turn
then move in the calculated direction
"""
func _process(delta):
	if !get_parent().turnable.myTurn:
		return
		
	var dir
		
	if(Input.is_action_just_released("w")):
		move_ctrl(Vector2(0,1))
	if(Input.is_action_just_released("a")):
		move_ctrl(Vector2(1,0))
	if(Input.is_action_just_released("s")):
		move_ctrl(Vector2(0,-1))
	if(Input.is_action_just_released("d")):
		move_ctrl(Vector2(-1,0))
		
