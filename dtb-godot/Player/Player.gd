"""
Player Script

contains player properties and methods to modify it
"""
extends Spatial
class_name Player

"""
on what tile player is
"""
export(Vector2) var onTile: Vector2

"""
Adds turnable behaviour to player, allows player to play turns
"""
onready var turnable: Turnable = Turnable.new(self, true)

"""
cache of `Gridable` node child
Manages tile movement
"""
onready var gridable = $Gridable

"""
Cache of `HUD` node
"""
# todo change to signals
onready var hud = get_node("/root/Game/HUD")

"""
health of the player
"""
export(int) var health :int = 5

"""
weapon durability
"""
export(int) var attackDurability :int = 0 setget set_durability

"""
number of keys player has
"""
export(int) var keys: int = 0 setget set_key

"""
jump height for animation when player is gets on one health
"""
export(float) var jumpHeight:float = 5

"""
Initializes properties and connects to signals
`move_done` method to `moved` signal in `Gridable`
`changedFacingDir` method to `faceDirChanged` in `Controllable`
"""
func _ready():
	$Gridable.connect("moved", self, "move_done")
	$Controllable.connect("faceDirChanged", $FaceRotate, "changedFacingDir")
	
	hud.set_hearts(health)
	set_durability(attackDurability)
	set_key(keys)
	set_physics_process(false)

"""
finishes the turn when move animation is finished
"""
func move_done():
	TurnSystem.UseMove(turnable)
	
"""
called by `Controllable` when attacking enemy
@param enemy enemy node object
"""
func damage(enemy):
	# play sfx
	SFX.PlayAxeHit()
	# cache the damage to deal
	var damage = attackDurability
	# reduce the enmy health by the damage and reduce the durability of the weapon
	self.attackDurability -= enemy.health
	enemy.hurt(damage)
	
	# if durability falls below 1, knock back the enemy
	if attackDurability < 0:
		self.attackDurability = 0
	if !enemy.isDead() && attackDurability == 0:
		enemy.knockback(enemy.onTile - onTile)
	
"""
called by enemy `AI` node
"""
func hurt():
	# play sfx and show redpulse animation on screen
	SFX.PlayDamage()
	get_node("/root/Game/HUD").red_pulse()
	
	# reduce health and update the hud
	health -= 1
	hud.set_hearts(health)
	
	# if the player gets on one health, do the jump animation
	if health == 1:
		jumpAnim()
		
	# if the player dies, show retry screen
	if health <= 0:
		hud.showRetry()

"""
setter calback for weapon durability
update hud
"""
func set_durability(val):
	if has_node("AxeModel"):
		$AxeModel.visible = val > 0
		hud.set_durability(val)
		
	attackDurability = val

"""
setter calback for keys
update hud
"""
func set_key(val):
	hud.set_key(val > 0)
	keys = val

"""
Jump animatioin when player gets to one health
"""
# optimize change to animation node instead of code
var t: float = 0
var jumpDir = 1
func jumpAnim():
	set_physics_process(true)
func _physics_process(delta):
	t += delta * jumpDir * 3
	
	translation = Vector3(\
	translation.x,\
	jumpHeight * t,\
	translation.z)
	
	if t >= 1:
		jumpDir = -1
	if t <= 0:
		set_physics_process(false)
		jumpDir = 1		


