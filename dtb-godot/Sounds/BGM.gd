"""
BGM Singleton
creates a instance of BGM scene to create BGM
"""
extends Node

func _ready():
	var bgm = load("res://Sounds/BGM.tscn").instance()
	add_child(bgm)
	bgm.set_owner(owner)