"""
SFX System

Singleton which can be used to play oneshot sounds
"""
extends Node

var audio: AudioStreamPlayer

onready var axeHit = preload("res://Sounds/Source/axehit.wav")
onready var damage = preload("res://Sounds/Source/damage.wav")
onready var doorunlocked = preload("res://Sounds/Source/doorunlocked.wav")
onready var pickup = preload("res://Sounds/Source/pickup.wav")

func _ready():
	audio = AudioStreamPlayer.new()
	audio.volume_db = -15
	add_child(audio)
	audio.set_owner(owner)
	
func PlayAxeHit():
	audio.stream = axeHit
	audio.play()
	
func PlayDamage():
	audio.stream = damage
	audio.play()
	
func PlayDoorunlocked():
	audio.stream = doorunlocked
	audio.play()
	
func PlayPickup():
	audio.stream = pickup
	audio.play()