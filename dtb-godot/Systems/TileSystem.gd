"""
TileSystem Singleton

Manages all the tiles in the map,
check if the tile is movable for players and enemies
"""
extends Node

"""
list of all the tiles
"""
var tiles: Array

"""
Used to calculate the spaces between each tile centers
Same as the one in `Gridable`
"""
var gridToPosFactor :float = 10

"""
Checks if the tile is movable using tile location

@return bool true if the tile is movable
"""
func IsMovable(tileLoc: Vector2) -> bool:
	return IsMovableTile(GetTile(tileLoc))
	
"""
Checks if the tile is movable using tile object

@return bool true if the tile is movable
"""
func IsMovableTile(t:Tile) -> bool:
	# checks if tile exists
	if t == null:
		return false
	
	# checks if the tile is occupied by someone
	# checks if the tile is not player (things can move on player)
	# check if the tile is a pickup
	# check if the tile is a door and if its open
	if t.IsOccupied() && \
	!(t.occupiedBy is Player) && \
	!t.occupiedBy.has_method("i_am_pickup") && \
	!(t.occupiedBy.has_method("i_am_door") && t.occupiedBy.isOpen)\
	:
		return false
	
	return true
	
"""
gets the tile object from tile location
uses lazy loading to load the tiles into a cache array
"""
func GetTile(tileLoc: Vector2) -> Tile:
	if tiles.size() == 0:
		tiles = get_node("/root/Game/Grid").get_children()
	
	for t in tiles:
		if t.tileLoc == tileLoc:
			return t
			
	return null
	
# move below methods to another system

"""
Restarts the level clears the tiles in the cache
"""
func Retry():
	call_deferred("destoryAndRetry")
	
"""
Restarts the game clears the tiles in the cache
"""
func Restart():
	call_deferred("destoryAndRestart")
	
func destoryAndRetry():
	tiles.clear()
	get_tree().reload_current_scene()
	
func destoryAndRestart():
	tiles.clear()
	get_tree().change_scene("res://Menu/MainMenu.tscn")