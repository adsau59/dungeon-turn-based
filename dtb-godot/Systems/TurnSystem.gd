"""
TurnSystem Singleton

manages turns of all the turnables in the game
"""
extends Node

"""
Signaled when turn is changed
@param turnable reference of the turnable object whos turn it is
"""
signal TurnChanged(turnable)

"""
array of all the turnables
"""
var turnables: Array

"""
index of the object from `turnables` array whos turn it is
"""
var currentTurnIndex = 0


"""
Add a new turnable to the array (used in the `Turnable` constructor)
"""
func AddTurnables(turnable):
	turnables.push_back(turnable)
	
	
"""
Called to finish the current turn and start the next turn, signas `TurnChanged1
"""
func UseMove(turnable):
	currentTurnIndex += 1
	if currentTurnIndex >= turnables.size():
		currentTurnIndex = 0
	
	if turnable != null:
		turnable.myTurn = false
		
	if turnables[currentTurnIndex].obj != null && \
	is_instance_valid(turnables[currentTurnIndex].obj) && \
	(!turnables[currentTurnIndex].obj is Enemy ||\
	 (turnables[currentTurnIndex].obj is Enemy && !turnables[currentTurnIndex].obj.isDead())):
		turnables[currentTurnIndex].myTurn = true
	else:
		turnables.remove(currentTurnIndex)
		currentTurnIndex -= 1
		UseMove(turnable)
		return
	
	emit_signal("TurnChanged", turnables[currentTurnIndex])