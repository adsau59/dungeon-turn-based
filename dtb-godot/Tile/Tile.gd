"""
Tile Script

Manages properties of tiles
"""

tool
extends Spatial
class_name Tile

"""
what is the location of this tile
"""
export(Vector2) var tileLoc: Vector2

"""
what object occupies this tile
"""
var occupiedBy

"""
default model cache of the tile
"""
var defaultModel

"""
enables a random tile from 4 different surfaces
"""
func _ready():
	if Engine.editor_hint:
		return
		
	randomize()
	var r = randi()%5
	
	$testing.visible = false
	
	$mod1.visible = r < 2
	$mod2.visible = r >= 2
	
	if r == 1:
		$mod1.rotation_degrees.z = 180
	if r == 4:
		$mod2.rotation_degrees.z = 180
		
	if r<2:
		defaultModel = $mod1
	else:
		defaultModel = $mod2

"""
Moves the tile to location
"""
# todo locToPosFactor hardcoded
func snapTo(loc: Vector2):
	translation = Vector3(10 * loc.x, translation.x, 10 * loc.y)
	tileLoc = loc
	
"""
Changes the object refence which occupies this tile
"""
func Occupy(obj):
	occupiedBy = obj

"""
checks if the tile is occupied
"""
func IsOccupied():
	return occupiedBy != null && is_instance_valid(occupiedBy)

"""
empties the tile
"""
func Leave():
	occupiedBy = null

"""
change the tile collor to red
"""
func detectionTile(show:bool):
	defaultModel.visible = !show
	$detectionTile.visible = show