"""
Wall Script

Changes different wall facing randomly in the start of the game,
also allows to enable/disable pillars on either side of the wall
"""

tool
extends Node

export(Vector2) var onTile: Vector2
export(bool) var orientation: bool
export(bool) var pillar1Visible:bool = false setget pillar1_changed
export(bool) var pillar2Visible:bool = false setget pillar2_changed

func _ready():
	if Engine.editor_hint:
		return
	
	# create a random number
	randomize()
	var r = randi()%5
	var mod
	
	# disable the default model
	$mod1.visible = false
	
	# show a wall depending on the random number
	if r < 2:
		$mod1.visible = true
		mod = $mod1
	elif r < 4 && r >= 2:
		$mod2.visible = true
		mod = $mod2
	elif r == 4:
		$mod3.visible = true
		mod = $mod3
	
	# rotate the wall to show different face
	if  r > 2:
		mod.rotation_degrees.y += 180
		
"""
callback from pillar1Visible
show/hide pillar1
"""
func pillar1_changed(val):
	$Pillar.visible = val
	pillar1Visible = val
	
"""
callback from pillar2Visible
show/hide pillar2
"""
func pillar2_changed(val):
	$Pillar2.visible = val
	pillar2Visible = val